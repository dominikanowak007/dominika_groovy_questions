app = new GroovyApp()
app.welcome()

app.createPerson()
app.printMap()
app.printList()
app.printSet()
app.showFindMethod()
app.showFindAllMethod()

app.printAllFilesInDirectory()

app.printAllFilesInDirectories()
app.print_HtmlGeneratedWith_Markupbuilder()