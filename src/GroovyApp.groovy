import groovy.xml.MarkupBuilder

class GroovyApp {

    void welcome(){

        println'Welcome'
    }

    void createPerson()
    {
        def person1= new PersonGroovy()
        person1.name = "Amanda"

        PersonJava person2 = new PersonJava()
        person2.name = "Amanda"
        println person1.name + '    '+  person2.getName()

    }

    void printMap(){

        def personMap = [:]
        personMap['First'] = 'John'
        personMap['Second'] = 'Ana'
        personMap['Third'] = 'Monica'

        println"First in the map is ${personMap['First']}"
        println 'Printing Map...'
         personMap.each{k,v-> println "${k}:${v}" }
        println()
    }

    void printList(){

        def nameList = []
        nameList.add("Johnny")
        nameList.add("Rebeca")
        nameList.add("Nobody")
        println 'Printing List...'
        nameList.each{println it + ", "}
        println()

    }

    void printSet(){

        Set<String> nameSet = ["Anna", "Berry", "Cathy"]
        assert nameSet instanceof Set
        println 'Printing Set...'
        nameSet.each{println it}
        println()
    }

    void showFindMethod(){
        def peopleList=[

        new PersonGroovy(name: "Ryba"),
        new PersonGroovy(name: "Natalia1"),
        new PersonGroovy(name: "Natalia2"),
        new PersonGroovy(name: "Natalia3"),
        new PersonGroovy(name: "Natalia4"),
        new PersonGroovy(name: "Natalia6")]


       def foundPerson = peopleList.find{p->p.name == 'Ryba'}
        println 'Printing Result of Find Method...'
        println foundPerson.name
        println()
    }
    void showFindAllMethod(){
        def peopleList=[

                new PersonGroovy(name: "Ryba"),
                new PersonGroovy(name: "Natalia1"),
                new PersonGroovy(name: "Natalia2"),
                new PersonGroovy(name: "Natalia3"),
                new PersonGroovy(name: "Natalia4"),
                new PersonGroovy(name: "Natalia6")]


        def foundPeople = peopleList.findAll{p->p.name.contains("Natal")}

        println 'Printing Result of FindAll Method...'
         foundPeople.each{p->println p.name}
        println()
    }

    void printAllFilesInDirectory() {
        String directoryName = '/Users/dominikanowak/Desktop'
        println "Printing given directory $directoryName"

        new File('/usr/local').eachFile() { f ->
            println f.getName()}

        println()
    }



    void printAllFilesInDirectories()
    {
        println "Printing given directory  and its subdirectories"
        List allFiles=[]
        File file = new File('/Users/dominikanowak/Desktop')

        if(file.isDirectory())
        {
            file.eachDir { sub -> sub.listFiles().each {f->println f.getName()} }
        }
        else if (file.isFile())
        {
          println  file.getName()
        }
    }

    // generated HTML can be tested at https://www.w3schools.com/html/tryit.asp?filename=tryhtml_intro

     void print_HtmlGeneratedWith_Markupbuilder() {

        def writer = new StringWriter()
        def html = new MarkupBuilder(writer)

        html.html {
            head ("This my home town")
            body(id: "main") {
                h2 ( "Home Town")
                p (
                    "Kobylin is a town in Krotoszyn County, Greater Poland, Poland, with 3,130 inhabitants."
                    )

                p(
                 "Gmina Kobylin is an urban-rural gmina (administrative district) in Krotoszyn County, " +
                         "Greater Poland Voivodeship, in west-central Poland. Its seat is the town of Kobylin, " +
                         "which lies approximately 14 kilometres (9 mi) west of Krotoszyn and 81 km (50 mi) " +
                         "south of the regional capital Poznań." )
                 p( "The gmina covers an area of 112.37 square kilometres (43.4 sq mi), and as of " +
                         "2006 its total population is 8,039 (out of which the population of Kobylin amounts to 3,084, " +
                         "and the population of the rural part of the gmina is 4,955)."
                )
                h2("Places in the town")
               p()
                a(href: "http://www.kobylin.pl/", "Kobylin Offical Page")
               p()
                a(href: "https://www.yelp.pl/biz/pizzeria-con-amore-kobylin", "Pizza")
               p()
                a(href: "http://palackobylin.pl/", "Hotel")
                p{img(src: "images/bee.png", border: 0.2)}
                p{img(src: "images/bunny.png", border: 0.2)}
            }
        }
         println writer
     }

}


